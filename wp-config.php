<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'massa');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'p B<gjZ40{)`[TcfWd]`w8a})P6sXHuKi>+w@F&TR=xy36D4{=7oZfis9TTL<D2j');
define('SECURE_AUTH_KEY',  'w2>uv2dB|c+}etcv65$vA@9?a*)!-=@Jkm@Cxr/Zt/]tfPOm/5)e~1ymjdke1Aa;');
define('LOGGED_IN_KEY',    'Z%cIG!Tm0Aay9`ut]Mm,[pDhF2h1Vl55.e@Jf!YAGcfvuQj)oP8C`h/=6wa^GVE{');
define('NONCE_KEY',        '3vj(C_U/R(6,KI&j,|l&Y3(GQytD8|b2[MW}/dgtv0hPDkfRad):JR#4|%F6)wl?');
define('AUTH_SALT',        'k<o92K;Je{*&fgpg3V#`RIy5+Nq3 7 <fm/=RfbDOnD/VWQOKJPe?RgB{-14?TyP');
define('SECURE_AUTH_SALT', ')<2fDh=qk_DS7t+7rho=yCYz]y;0(0DuVXgv2ln@R/g25hxi8F[Bm(LKwpsK:#r1');
define('LOGGED_IN_SALT',   '@U:l@3n6oHf3+KFNr}sA>XV1-4WeB%8ww*>_t1?|!r04cI+MI44;n<d](y~ntz)5');
define('NONCE_SALT',       'zk&wh!X[e#D-$ngvex+$+eAM[y2DYeBgvn7rs;>4}+Ru`6bk#~h+^Q_{<^{gjr(c');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
