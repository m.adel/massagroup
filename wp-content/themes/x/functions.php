<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Theme functions for X.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Set Paths
//   02. Require Files
// =============================================================================

// Set Paths
// =============================================================================

$func_path = 'framework/functions';
$glob_path = 'framework/functions/global';
$admn_path = 'framework/functions/global/admin';
$eque_path = 'framework/functions/global/enqueue';
$plgn_path = 'framework/functions/global/plugins';



// Require Files
// =============================================================================


//
// Debugging, conditionals, helpers, and stack data.
//

require_once( $glob_path . '/debug.php' );
require_once( $glob_path . '/conditionals.php' );
require_once( $glob_path . '/helper.php' );
require_once( $glob_path . '/stack-data.php' );
require_once( $glob_path . '/tco-setup.php' );


//
// Admin.
//

require_once( $admn_path . '/thumbnails/setup.php' );
require_once( $admn_path . '/setup.php' );
require_once( $admn_path . '/migration.php' );
require_once( $admn_path . '/meta/setup.php' );
require_once( $admn_path . '/sidebars.php' );
require_once( $admn_path . '/widgets.php' );
require_once( $admn_path . '/custom-post-types.php' );
require_once( $admn_path . '/customizer/setup.php' );
require_once( $admn_path . '/addons/setup.php' );


//
// Enqueue styles and scripts.
//

require_once( $eque_path . '/styles.php' );
require_once( $eque_path . '/scripts.php' );


//
// Global functions.
//

require_once( $glob_path . '/meta.php' );
require_once( $glob_path . '/featured.php' );
require_once( $glob_path . '/pagination.php' );
require_once( $glob_path . '/navbar.php' );
require_once( $glob_path . '/breadcrumbs.php' );
require_once( $glob_path . '/classes.php' );
require_once( $glob_path . '/portfolio.php' );
require_once( $glob_path . '/social.php' );
require_once( $glob_path . '/content.php' );
require_once( $glob_path . '/remove.php' );


//
// Stack specific functions.
//

require_once( $func_path . '/integrity.php' );
require_once( $func_path . '/renew.php' );
require_once( $func_path . '/icon.php' );
require_once( $func_path . '/ethos.php' );


//
// Integrated plugins.
//

require_once( $plgn_path . '/cornerstone.php' );

if ( X_BBPRESS_IS_ACTIVE ) {
  require_once( $plgn_path . '/bbpress.php' );
}

if ( X_BUDDYPRESS_IS_ACTIVE ) {
  require_once( $plgn_path . '/buddypress.php' );
}

if ( X_CONVERTPLUG_IS_ACTIVE ) {
  require_once( $plgn_path . '/convertplug.php' );
}

if ( X_ENVIRA_GALLERY_IS_ACTIVE ) {
  require_once( $plgn_path . '/envira-gallery.php' );
}

if ( X_ESSENTIAL_GRID_IS_ACTIVE ) {
  require_once( $plgn_path . '/essential-grid.php' );
}

if ( X_LAYERSLIDER_IS_ACTIVE ) {
  require_once( $plgn_path . '/layerslider.php' );
}

if ( X_REVOLUTION_SLIDER_IS_ACTIVE ) {
  require_once( $plgn_path . '/revolution-slider.php' );
}

if ( X_SOLILOQUY_IS_ACTIVE ) {
  require_once( $plgn_path . '/soliloquy.php' );
}

if ( X_VISUAL_COMOPSER_IS_ACTIVE ) {
  require_once( $plgn_path . '/visual-composer.php' );
}

if ( X_WOOCOMMERCE_IS_ACTIVE ) {
  require_once( $plgn_path . '/woocommerce.php' );
}

if ( X_WPML_IS_ACTIVE ) {
  require_once( $plgn_path . '/wpml.php' );
}

add_action( 'template_redirect', 'wc_custom_redirect_after_purchase' );
function wc_custom_redirect_after_purchase() {
	global $wp;

	// if ( is_checkout() && ! empty( $wp->query_vars['order-received'] ) ) {
	// 	wp_redirect(  get_site_url().'/client' );
	// 	exit;
	// }
}


add_action( 'woocommerce_check_cart_items', 'spyr_set_min_qty_per_product' );
function spyr_set_min_qty_per_product() {
	// Only run in the Cart or Checkout pages
	if( is_cart() || is_checkout() ) {
		global $woocommerce;

		// Product Id and Min. Quantities per Product
		$product_min_qty = array(
			array( 'id' => 223, 'min' => 10, 'max' => 50),
			array( 'id' => 222, 'min' => 50,'max' => 100),
			array( 'id' => 224, 'min' => 100 ),
		);

		// Will increment
		$i = 0;
		// Will hold information about products that have not
		// met the minimum order quantity
		$bad_products = array();

		// Loop through the products in the Cart
		foreach( $woocommerce->cart->cart_contents as $product_in_cart ) {
			// Loop through our minimum order quantities per product
			foreach( $product_min_qty as $product_to_test ) {
				// If we can match the product ID to the ID set on the minimum required array
				if( $product_to_test['id'] == $product_in_cart['product_id'] ) {

					// If the quantity required is less than than the quantity in the cart now
					if(!(($product_to_test['min'] <= $product_in_cart['quantity']) && ($product_in_cart['quantity'] <= $product_to_test['max'])))
					{

						// Get the product ID
						$bad_products[$i]['id'] = $product_in_cart['product_id'];
						// Get the Product quantity already in the cart for this product
						$bad_products[$i]['in_cart'] = $product_in_cart['quantity'];
						// Get the minimum required for this product

						if($product_to_test['min'] >= $product_in_cart['quantity']){
							$bad_products[$i]['qty_type'] = 'minimum';
							$bad_products[$i]['qty_amount'] = $product_to_test['min'];
						}
						else if($product_in_cart['quantity'] >= $product_to_test['max']){
							$bad_products[$i]['qty_type'] = 'maximum';
							$bad_products[$i]['qty_amount'] = $product_to_test['max'];
						}
						//$bad_products[$i]['min_req'] = $product_to_test['min'];
						//$bad_products[$i]['max_req'] = $product_to_test['max'];
					}
				}
			}
			// Increment $i
			$i++;
		}

		// Time to build our error message to inform the customer
		// About the minimum quantity per order.
		if( is_array( $bad_products) && count((array) $bad_products ) > 0 ) {

			// Lets begin building our message
			$message = '';//'<strong>A minimum quantity per product has not been met.</strong><br />';
			foreach( $bad_products as $bad_product ) {
				// Append to the current message
				$message .= get_the_title( $bad_product['id'] ) .' requires a '.$bad_product['qty_type'].' quantity of '
						 . $bad_product['qty_amount']
						 .'. You currently have: '. $bad_product['in_cart'] .'.<br />';
			}
			wc_add_notice( $message, 'error' );
			echo'<script type="text/javascript">jQuery( document ).ready(function() {jQuery(".wc-proceed-to-checkout a").click(function(e){e.preventDefault();jQuery("html, body").animate({scrollTop : 0},800); jQuery(".calculated_shipping").fadeOut()});});</script>';
		}

	}
}

add_action('wp_head', function(){ echo '<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" >'; }, 99999 );
function upcoming_movies() {
  $output = '';
  $args = array(
    'post_type'=> 'upcoming',
    'order'    => 'DESK',
    'posts_per_page' => 1
);
query_posts( $args );
while ( have_posts() ) : the_post();
$staf_meta = get_post_meta( get_the_ID(), 'staf', true );
$up_author = get_post_meta( get_the_ID(), 'up_author', true );
$director = get_post_meta( get_the_ID(), 'director', true );
$teaser = get_post_meta( get_the_ID(), 'teaser', true );
$teaserAdditional = get_post_meta( get_the_ID(), 'teaser_additional', true );
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
$output .= '<div class="wpb_row upcome_blc" style="background-image:url('.$image[0].'); padding:5% 0; background-size: cover;">';
$output .= '<div class="x-container max width">';
$output .= '<div class="up_cont">';
$output .= '<div class="x-column x-1-2">';
$output .= '<h3>Upcoming</h3>';
$output .= '<h1>'.get_the_title().'</h1>';
$output .= '<p><i class="fa fa-users up_dets"></i>'.$staf_meta.'</p>';
$output .= '<p><i class="fa fa-pencil up_dets"></i>'.$up_author.'</p>';
$output .= '<p><i class="fa fa-film up_dets"></i>'.$director.'</p>';
$output .= '</div>';
$output .= '<div class="x-column x-1-2" style="text-align: right;">';
if($teaser){
  $output .= '<a class="teaserBtn" target="_blank" href="'.$teaser.'"><i class="fa fa-play-circle ply_now"></i>Trailer</a>';
  if($teaserAdditional){
    $output .= '<a class="teaserBtn" target="_blank" href="'.$teaserAdditional.'"><i class="fa fa-play-circle ply_now"></i>Trailer 2</a>';
  }
}

$output .= '</div>';
$output .= '</div>';
$output .= '</div>';
$output .= '</div>';

endwhile;
wp_reset_query();
return $output;
}

add_shortcode( 'upcoming', 'upcoming_movies' );

function team_function() {
  $output = '';
  $args = array(
    'post_type'=> 'team',
    'order'    => 'DESK',
    'posts_per_page' => 3
);
query_posts( $args );
while ( have_posts() ) : the_post();
$position = get_post_meta( get_the_ID(), 'position', true );
$phone = get_post_meta( get_the_ID(), 'phone', true );
$facebook_account = get_post_meta( get_the_ID(), 'facebook_account', true );
$twitter_account = get_post_meta( get_the_ID(), 'twitter_account', true );
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
$output .= '<div class="x-column x-1-3">';
$output .= '<div class="team_cont"><img src="'.$image[0].'" alt="'.get_the_title().'"/>
<div class="data_cont">
  <h4>'.get_the_title().'</h4>
  <i>'.$position.'</i><br/><br/>';
  if ($facebook_account){
    $output .= '<a href="mailto:'.$facebook_account.'"><i class="fa fa-envelope"></i></a>';
  }


$output .= '</div>

</div>';
$output .= '</div>';
endwhile;
wp_reset_query();
return $output;
}

add_shortcode( 'team', 'team_function' );

//Cinemas listing grid ShortCode

function cinemas_function() {
  $output = '';
  $args = array(
    'post_type'=> 'cinema',
    'order'    => 'Asc',
    'posts_per_page' => -1
);
query_posts( $args );
$output .= '<div class="innr_cont" style="padding: 50px 0;">';
$output .= '<div class="x-container">';
while ( have_posts() ) : the_post();
$location = get_post_meta( get_the_ID(), 'location', true );
$screens = get_post_meta( get_the_ID(), 'screens', true );
$seats = get_post_meta( get_the_ID(), 'seats', true );
$output .= '<div class="x-column x-1-3 lessMarg"><div class="cinemaContainer">';
$output .= '<h2>'.get_the_title().'</h2>';
$output .= '<i class="vc_btn3-icon fa fa-location-arrow"></i>&nbsp;<span>'.$location.'</span><br/>';
$output .= '<div class="cinemaData">';
$output .= '<div class="counterTime"><i>Screens</i><span class="cimaDetailsNo">'.$screens.'</span></div>';
$output .= '<div class="counterTime"><i>Seats</i><span class="cimaDetailsNo">'.$seats.'</span></div>';
$output .= '</div>';
$output .= '</div>';
$output .= '</div>';
endwhile;
wp_reset_query();
$output .= '</div>';
$output .= '</div>';
return $output;
}

add_shortcode( 'cinemasListing', 'cinemas_function' );



// Production grid ShortCode
function gal_function() {
  $output = '';
  if(is_page('production')){
    $trm = 'production';
  } else{
    $trm = 'distribution';
  }
  $args = array(
    'post_type'=> 'movie',
    'order'    => 'ASC',
    'posts_per_page' => -1,
    'tax_query' => array(
		array(
      'taxonomy' => 'category',
			'field' => 'slug',
			'terms' => $trm
		),
	),
);

query_posts( $args );
while ( have_posts() ) : the_post();
$poster = get_post_meta( get_the_ID(), 'poster', true );
$trailer = get_post_meta( get_the_ID(), 'trailer', true );
$prod_year = get_post_meta( get_the_ID(), 'prod_year', true );
$playing_date = get_post_meta( get_the_ID(), 'playing_date', true );
$genre = get_post_meta( get_the_ID(), 'genre', true );
$staff = get_post_meta( get_the_ID(), 'staff', true );
$direction = get_post_meta( get_the_ID(), 'direction', true );
$scenario = get_post_meta( get_the_ID(), 'scenario', true );

if($trm == 'production'){
  $type_of_production = get_post_meta( get_the_ID(), 'type_of_production', true );
} else {
    $type_of_production = get_post_meta( get_the_ID(), 'type_of_distribution', true );
}
if ($type_of_production) {
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
$output .= '<div class="x-column x-1-6 mov_bg" style="position:relative;    overflow: hidden;">';
$output .= '<a href="#my-content'.get_the_ID().'" class="lightbox-selector" data-type="inline"><img src="'.$poster['guid'].'" alt="'.get_the_title().'"/></a>';
//var_dump($poster['guid']);

$output .= '<div class="details_cont"><h2><a href="#my-content'.get_the_ID().'" class="lightbox-selector" data-type="inline">'.get_the_title().'</a></h2>';

if($type_of_production == 'production'){
  $output .= '<i class="fa fa-cogs"></i>&nbsp;<i>'.$type_of_production.'</i>';

} else{
  $output .= '<i class="fa fa-share-alt"></i>&nbsp;<i>'.$type_of_production.'</i>';
}
$output .= '</div></div>';
$output .= '<div id="my-content'.get_the_ID().'" class="the_cont_hldr"><h1>'.get_the_title().'</h1>
<div class="row">
<div class="x-column x-3-4 mov_det_hldr">
<div>
'.get_the_content().'
<p>
<span class="vc_icon_element-icon fa fa-calendar"></span>&nbsp;'.$prod_year.'<br />
<span class="vc_icon_element-icon fa fa-film"></span>&nbsp;'.$genre.'<br />
<span class="vc_icon_element-icon fa fa-users"></span>&nbsp;'.$staff.'<br />
<span class="vc_icon_element-icon fa fa-video-camera"></span>&nbsp;'.$direction.'<br />
<span class="vc_icon_element-icon fa fa-pencil"></span>&nbsp;'.$scenario.'<br />
</p>
</div>
</div>
<div class="x-column x-1-4">
<img src="'.$poster['guid'].'" alt="'.get_the_title().'"/>';
if($trailer):
$output .= '<a class="ply_btn" target="_blank" href="'.$trailer.'"><span class="vc_icon_element-icon fa fa-play"></span>&nbsp;Play Trailer</a>';
endif;
$output .= '</div>
</div>
</div>';
};
endwhile;
$output .= '<div id="my-content" class="pop_cont"><i id="cls_ic">+</i><div id="inr_cont_data">

</div></div>';
wp_reset_query();
return $output;

}
add_shortcode( 'production', 'gal_function' );
