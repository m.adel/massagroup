<?php

// =============================================================================
// HEADER.PHP
// -----------------------------------------------------------------------------
// The site header. Variable output across different stacks.
//
// Content is output based on which Stack has been selected in the Customizer.
// To view and/or edit the markup of your Stack's index, first go to "views"
// inside the "framework" subdirectory. Once inside, find your Stack's folder
// and look for a file called "wp-header.php," where you'll be able to find the
// appropriate output.
// =============================================================================

?>

<?php x_get_view( x_get_stack(), 'wp', 'header' ); ?>
<?php if(is_front_page()):?>
<ul id="vert_nav">
    <li>
      <a href="#rev_slider_1_1_forcefullwidth"><span>Home</span></a>
    </li>
    <li>
      <a href="#about" data-number="1"><span>About</span></a>
    </li>
    <li>
    <a href="#wht_do" data-number="2"><span>What we Do</span></a>
    </li>
    <li>
    <a href="#upcoming" data-number="3"><span>Upcoming</span></a>
    </li>
    <li>
    <a href="#team" data-number="4"><span>Team</span></a>
    </li>
    <li>
    <a href="#contact" data-number="5"><span>Contact us</span></a>
  </li>
</ul>
<?php endif;?>
