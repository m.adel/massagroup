window.NileTaxi = {

	// Object name:
	name	: 'NileTaxi',

	// Version:
	version	: '1.0.0',

	// Detect user agent:
	UA: new UAParser(),

	// Available from locations:
	availableLocations_FROM: ["Loading..."],

	// Available to locations:
	availableLocations_TO: ["Loading..."],

	// Available zones
	availableZones: [
		{
			name: 'Zone 1',
			priceMap: [10, 15, 20, 25, 30, 30, 35]
		},
		{
			name: 'Zone 2',
			priceMap: [15, 10, 15, 20, 25, 25, 35]
		},
		{
			name: 'Zone 3',
			priceMap: [20, 15, 10, 15, 20, 20, 30]
		},
		{
			name: 'Zone 4',
			priceMap: [25, 20, 15, 10, 15, 15, 20]
		},
		{
			name: 'Zone 5 A',
			priceMap: [30, 25, 20, 15, 10, 15, 20]
		},
		{
			name: 'Zone 5 B',
			priceMap: [35, 30, 25, 20, 15, 10, 15]
		},
		{
			name: 'Zone 6',
			priceMap: [35, 30, 25, 20, 15, 15, 10]
		}
	],

	freeTimesForOnCall: [
				{
					remining_quota: '',
					taxi_quota: '',
					time: '8:00 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '8:30 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '9:00 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '9:30 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '10:00 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '10:30 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '11:00 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '11:30 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '12:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '12:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '1:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '1:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '2:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '2:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '3:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '3:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '4:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '4:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '5:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '5:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '6:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '6:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '7:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '7:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '8:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '8:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '9:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '9:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '10:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '10:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '11:00 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '11:30 PM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '12:00 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '12:30 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '1:00 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '1:30 AM'
				}, {
					remining_quota: '',
					taxi_quota: '',
					time: '2:00 AM'
	}]
};
