<?php

// =============================================================================
// TEMPLATE NAME: Layout - CheckOut
// -----------------------------------------------------------------------------
// Handles output of fullwidth pages.
//
// Content is output based on which Stack has been selected in the Customizer.
// To view and/or edit the markup of your Stack's index, first go to "views"
// inside the "framework" subdirectory. Once inside, find your Stack's folder
// and look for a file called "template-layout-full-width.php," where you'll be
// able to find the appropriate output.
// =============================================================================

?>

<?php get_header(); ?>

<div class="x-main full" role="main">

    
	<article id="post-4" class="post-4 page type-page status-publish hentry no-post-thumbnail">
		<div class="entry-content content">
		<div id="x-content-band-1" class="x-content-band vc man" style="background-color: #ffffff; padding-top: 2%; padding-bottom: 2%;">
		<div class="x-container max width wpb_row">
			<?php do_action( 'woocommerce_checkout_order_review' ); ?>

		</div>
		</div>


		</div>

      </article>

    
</div>
<?php get_footer(); ?>